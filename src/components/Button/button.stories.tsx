import type { Meta, StoryObj } from '@storybook/react';

import { Button } from './';

const meta = {
  argTypes: {
    onClick: { action: 'clicked' },
  },
  component: Button,
  tags: ['autodocs'],
  title: 'Components/Button',
} satisfies Meta<typeof Button>;

export default meta;
type Story = StoryObj<typeof meta>;

export const BaseButton: Story = {
  args: {
    children: 'Styled Button',
    disabled: false,
  },
};

export const DisabledButton: Story = {
  args: {
    children: 'Disabled Button',
    disabled: true,
  },
};
