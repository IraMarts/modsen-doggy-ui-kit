import { styled } from 'styled-components';

import colors from '../../variables/colors';

export const Button = styled.button`
  all: unset;

  cursor: pointer;

  box-sizing: border-box;
  border-radius: 30px;
  background-color: ${colors.secondaryBackground};

  height: 48px;
  padding: 10px 16px;
  justify-content: center;
  align-items: center;

  color: ${colors.secondaryText};
  font-style: normal;
  text-align: center;
  font-size: 20px;
  font-weight: 700;
  line-height: 1.05;

  &:focus-visible {
    outline: 2px solid ${colors.primaryText};
    outline-offset: 2px;
  }

  &:hover {
    background-color: ${colors.accentBackground};
    color: ${colors.primaryText};
  }

  &:active {
    background-color: ${colors.terciaryBackground};
    color: ${colors.primaryText};
  }

  &:disabled {
    opacity: 0.6;
  }
`;
