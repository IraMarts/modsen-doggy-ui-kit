export default {
  accentBackground: '#DFB2A9', // rosey cheek темный футер, рамка картинки
  inputBorder: '#E0E0E0',
  pink: '#FFDAE9', // in-house
  primaryBackground: '#F3DED7', // in-house фон картинокб футера
  primaryText: '#4c4c4b', //charcoal
  secondaryBackground: '#e89b93', // pink kisses  кнопка
  secondaryText: '#ffffff',
  terciaryBackground: '#b78e80', // bronzed rose
};
